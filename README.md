## wine容器专用额外自体

### 使用方法

1. 点击下载，下载格式zip或者tar.gz都行。

2. 解压。

3. 根据你的容器名称，将字体复制到容器里面。然后重启wine对应的应用即可。

   eg:

   ```bash
   # 列出当前所有容器
   ls ~/.deepinwine
   # 复制字体到目标容器
   export container_name="container_for_tlntin_amd64"
   cp Fonts/* "/home/${USER}/.deepinwine/${container_name}/drive_c/windows/Fonts"
   ```
   
   